package menu

import (
	"testing"
)

func TestMenuItemToString(t *testing.T) {
	infoItem := Item{
		ItemName: 1,
		label:    "Info",
	}
	infoStr := infoItem.ToString()
	if infoStr != "1. Info." {
		t.Errorf("info.ToString() = %s; want '1. Info.'", infoStr)
	}
}

func TestMenuToString(t *testing.T) {
	menuStr := CreateMenu().ToString()
	expMenuStr := "1. Info.\n2. Deposit.\n3. Withdraw.\n4. Exit."
	if menuStr != expMenuStr {
		t.Errorf("menu.ToString() = \n%s\n; want:\n%s\n", menuStr, expMenuStr)
	}
}

func TestSelectSuccess(t *testing.T) {
	m := CreateMenu()
	numberToLabel := map[int]string{
		1: "Info",
		2: "Deposit",
		3: "Withdraw",
		4: "Exit",
	}
	for idx, expLabel := range numberToLabel {
		t.Run("Select 1", func(t *testing.T) {
			selection := m.Select(idx)
			if selection.ItemName != ItemName(idx) {
				t.Errorf("Select(%d) is: %d; Expected is: %s", idx, selection.ItemName, expLabel)
			}
		})
	}
}

func TestSelectIncorrectNumber(t *testing.T) {
	m := CreateMenu()
	t.Run("0 index is incorrect for Menu", func(t *testing.T) {
		testIncorrectNumberErrorMsg(t, 0, m)
	})
	t.Run("5 index is incorrect for Menu", func(t *testing.T) {
		testIncorrectNumberErrorMsg(t, 5, m)
	})
}

func testIncorrectNumberErrorMsg(t *testing.T, index int, m *Menu) {
	result := m.Select(index)
	if result.ItemName != None {
		t.Errorf("Should return 'none' menu item. Actual: %s", result.ToString())
	}
}
