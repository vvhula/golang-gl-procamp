package menu

import "fmt"

const (
	Info ItemName = iota + 1
	Deposit
	Withdraw
	Exit
	None
)

type (
	ItemName int

	Item struct {
		ItemName ItemName
		label    string
	}

	Menu struct {
		info     Item
		deposit  Item
		withdraw Item
		exit     Item
	}
)

func CreateMenu() *Menu {
	return &Menu{
		info:     Item{Info, "Info"},
		deposit:  Item{Deposit, "Deposit"},
		withdraw: Item{Withdraw, "Withdraw"},
		exit:     Item{Exit, "Exit"},
	}
}

func (item Item) ToString() string {
	return fmt.Sprintf("%d. %s.", item.ItemName, item.label)
}

func (menu *Menu) items() []Item {
	return []Item{menu.info, menu.deposit, menu.withdraw, menu.exit}
}

func (menu *Menu) ToString() (asString string) {
	for i, item := range menu.items() {
		asString = asString + item.ToString()
		if i < (len(menu.items()) - 1) {
			asString = asString + "\n"
		}
	}
	return
}

func (menu *Menu) Select(itemNumber int) *Item {
	for _, item := range menu.items() {
		if item.ItemName == ItemName(itemNumber) {
			return &item
		}
	}
	return &Item{None, "Wrong input"}
}
