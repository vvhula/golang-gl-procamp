package main

import (
	"bitbucket.org/vvhula/golang-gl-procamp/atm"
)

func main() {
	account := atm.EmptyAccount()
	isFinishedChan := make(chan bool)
	valuesChan := make(chan string)
	session := atm.CreateSession(account, isFinishedChan, valuesChan)
	session.Operate()
	close(isFinishedChan)
	close(valuesChan)
}
