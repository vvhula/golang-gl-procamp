package atm

import (
	. "bitbucket.org/vvhula/golang-gl-procamp/menu"
	. "bitbucket.org/vvhula/golang-gl-procamp/money"
	"bufio"
	"fmt"
	"math/big"
	"os"
	"strconv"
	"strings"
)

type (
	deposit struct {
		currency     Currency
		qty          *big.Int
		denomination *big.Int
	}

	deposits []*deposit

	Account struct {
		deps *deposits
	}

	Session struct {
		atm               *Machine
		account           *Account
		valuesChannel     chan string
		isFinishedChannel chan bool
		finished          bool
	}

	Machine struct {
		menu *Menu
	}

	OperationError struct {
		msg string
	}

	ParsingError struct {
		value string
		msg   string
	}
)

func CreateATM() *Machine {
	return &Machine{menu: CreateMenu()}
}

func CreateSession(account *Account, isFinishedChannel chan bool, valuesChannel chan string) *Session {
	return &Session{atm: CreateATM(), account: account, isFinishedChannel: isFinishedChannel, valuesChannel: valuesChannel}
}

func EmptyAccount() *Account {
	return &Account{deps: &deposits{}}
}

func wrongInput() {
	fmt.Println("Wrong input")
}

func (session *Session) info() {
	fmt.Println("You have available:")
	fmt.Printf("%d %s\n", session.account.balance(USD), USD)
	fmt.Printf("%d %s\n", session.account.balance(EUR), EUR)
	fmt.Printf("%d %s\n", session.account.balance(UAH), UAH)
}

func (dep deposit) sum() *big.Int {
	one := new(big.Int).SetInt64(1)
	return one.Mul(dep.qty, dep.denomination)
}

func (deps deposits) contains(currency Currency) bool {
	for _, dep := range deps {
		if dep.currency == currency {
			return true
		}
	}
	return false
}

func (deps deposits) sum(currency Currency) *big.Int {
	result := new(big.Int).SetInt64(0)
	for _, dep := range deps {
		if dep.currency == currency {
			result = result.Add(result, dep.sum())
		}
	}
	return result
}

func parseDeposits(input string) (deps *deposits, err error) {
	result := deposits{}
	parsedInput := strings.TrimSuffix(input, ";")
	if !strings.HasSuffix(input, ";") {
		err = ParsingError{msg: "missing suffix ;", value: input}
		return
	} else if strings.HasPrefix(input, ";") {
		err = ParsingError{msg: "unexpected prefix ;", value: input}
		return
	}
	if strings.HasSuffix(parsedInput, ";") {
		err = ParsingError{msg: "too many semicolons", value: input}
		return
	}
	semicolonSeparatedTokens := strings.Split(parsedInput, ";")
	if len(semicolonSeparatedTokens) <= 0 {
		err = ParsingError{msg: "unexpected value", value: input}
		return
	}
	for _, token := range semicolonSeparatedTokens {
		if strings.TrimSpace(token) == "" {
			err = ParsingError{msg: "too many semicolons", value: input}
			return
		}
		dep, parsingErr := parseDeposit(token)
		if parsingErr != nil {
			err = parsingErr
			return
		}
		result = append(result, dep)
	}
	deps = &result
	return
}

func parseDeposit(input string) (dep *deposit, err error) {
	spaceSeparatedTokens := strings.Split(input, " ")
	if len(spaceSeparatedTokens) != 2 {
		err = ParsingError{msg: "expected two tokens separated by space", value: input}
		return
	}
	operationToken := spaceSeparatedTokens[0]
	currencyToken := spaceSeparatedTokens[1]
	currency := ParseCurrency(currencyToken)
	if !currency.IsValid() {
		err = ParsingError{msg: "unsupported currency", value: currencyToken}
		return
	}
	numbers := strings.Split(operationToken, "*")
	if len(numbers) != 2 || numbers[0] == "" || numbers[1] == "" {
		err = ParsingError{msg: "expected two numbers separated by *", value: operationToken}
		return
	}
	if strings.HasPrefix(numbers[0], "0") {
		err = ParsingError{msg: "qty cannot start with zero", value: numbers[0]}
		return
	}
	if strings.HasPrefix(numbers[1], "0") {
		err = ParsingError{msg: "denomination cannot start with zero", value: numbers[1]}
		return
	}
	qty, ok := new(big.Int).SetString(numbers[0], 10)
	if !ok || qty.Cmp(big.NewInt(0)) != 1 {
		err = ParsingError{msg: "qty is not valid", value: numbers[0]}
		return
	}
	denomination, ok := new(big.Int).SetString(numbers[1], 10)
	if !ok {
		err = ParsingError{msg: "denomination is not valid", value: numbers[1]}
		return
	}
	if !currency.IsDenominationAllowed(denomination) {
		err = ParsingError{msg: "denomination is not supported by this currency " + string(currency), value: numbers[1]}
		return
	}
	dep = &deposit{currency: currency, qty: qty, denomination: denomination}
	return
}

func (session *Session) withdraw() {
	fmt.Println("Withdraw")
}

func (session *Session) exit() {
	session.finished = true
}

func (session *Session) ShowMenu() {
	fmt.Println("Choose next operation:")
	fmt.Println(session.atm.menu.ToString())
}

func (err OperationError) Error() string {
	return fmt.Sprintf("Error: %s", err.msg)
}

func (err ParsingError) Error() string {
	return fmt.Sprintf("Parsing error: %s. Incorrect value: %s", err.msg, err.value)
}

func (session *Session) Operate() {
	session.ShowMenu()
	go func() {
		_ = session.useChannel()
	}()
	scanner := bufio.NewScanner(os.Stdin)
	go func() {
		for scanner.Scan() {
			input := scanner.Text()
			session.valuesChannel <- input
		}
	}()
	for {
		finished := <-session.isFinishedChannel
		if finished {
			break
		}
	}
}

func (session *Session) useChannel() (err error) {
	if session.finished {
		err = OperationError{"session is finished"}
		return
	}
	for {
		input := <-session.valuesChannel
		number, _ := strconv.Atoi(input)
		item := session.atm.menu.Select(number)
		switch item.ItemName {
		case Info:
			session.info()
		case Deposit:
			fmt.Println("Please, put your money.")
			text := <-session.valuesChannel
			deps, err := parseDeposits(text)
			if err == nil {
				if deps.contains(USD) {
					fmt.Printf("%d %s\n", deps.sum(USD), USD)
				}
				if deps.contains(EUR) {
					fmt.Printf("%d %s\n", deps.sum(EUR), EUR)
				}
				if deps.contains(UAH) {
					fmt.Printf("%d %s\n", deps.sum(UAH), UAH)
				}
				session.account.deposit(deps)
			} else {
				fmt.Println("Your operation can't be processed.")
			}
		case Withdraw:
			session.withdraw()
		case Exit:
			session.exit()
		case None:
			wrongInput()
		}
		if session.finished {
			session.isFinishedChannel <- true
			fmt.Println("Bye!")
			break
		} else {
			session.isFinishedChannel <- false
		}
		session.ShowMenu()
	}
	return
}

func (account Account) balance(currency Currency) *big.Int {
	return account.deps.sum(currency)
}

func (account *Account) deposit(deps *deposits) {
	newDeposits := append(append(deposits{}, *deps...), *account.deps...)
	account.deps = &newDeposits
}
