package atm

import (
	. "bitbucket.org/vvhula/golang-gl-procamp/money"
	"fmt"
	"math/big"
	"testing"
)

var parseDepositErrorsTable = []struct {
	in     string
	errMsg string
}{
	{"1*10", "Parsing error: expected two tokens separated by space. Incorrect value: 1*10"},
	{"1*", "Parsing error: expected two tokens separated by space. Incorrect value: 1*"},
	{"1", "Parsing error: expected two tokens separated by space. Incorrect value: 1"},
	{"1 UAH", "Parsing error: expected two numbers separated by *. Incorrect value: 1"},
	{"1*10 PLN U", "Parsing error: expected two tokens separated by space. Incorrect value: 1*10 PLN U"},
	{"1*10 PLN", "Parsing error: unsupported currency. Incorrect value: PLN"},
	{"1*2*10 UAH", "Parsing error: expected two numbers separated by *. Incorrect value: 1*2*10"},
	{"1+10 UAH", "Parsing error: expected two numbers separated by *. Incorrect value: 1+10"},
	{"*10 UAH", "Parsing error: expected two numbers separated by *. Incorrect value: *10"},
	{"10* UAH", "Parsing error: expected two numbers separated by *. Incorrect value: 10*"},
	{"10 UAH", "Parsing error: expected two numbers separated by *. Incorrect value: 10"},
	{"f*10 UAH", "Parsing error: qty is not valid. Incorrect value: f"},
	{"10*f UAH", "Parsing error: denomination is not valid. Incorrect value: f"},
	{"10*12 UAH", "Parsing error: denomination is not supported by this currency UAH. Incorrect value: 12"},
	{"10* 12 UAH", "Parsing error: expected two tokens separated by space. Incorrect value: 10* 12 UAH"},
	{"10 * 12 UAH", "Parsing error: expected two tokens separated by space. Incorrect value: 10 * 12 UAH"},
	{"10 *12 UAH", "Parsing error: expected two tokens separated by space. Incorrect value: 10 *12 UAH"},
	{" 10*12 UAH", "Parsing error: expected two tokens separated by space. Incorrect value:  10*12 UAH"},
	{"10*12 UAH ", "Parsing error: expected two tokens separated by space. Incorrect value: 10*12 UAH "},
	{"10*-1 UAH", "Parsing error: denomination is not supported by this currency UAH. Incorrect value: -1"},
	{"01*10 UAH", "Parsing error: qty cannot start with zero. Incorrect value: 01"},
	{"1*010 UAH", "Parsing error: denomination cannot start with zero. Incorrect value: 010"},
	{"1*-922337203685477580790 UAH", "Parsing error: denomination is not supported by this currency UAH. Incorrect value: -922337203685477580790"},
}

func TestParseDeposit(t *testing.T) {
	t.Parallel()
	for _, entry := range parseDepositErrorsTable {
		dep, err := parseDeposit(entry.in)
		if dep != nil {
			t.Errorf("No readDeposits is expected")
		} else {
			verifyParsingError(t, entry.errMsg, err)
		}
	}
}

func TestParseDepositBillions(t *testing.T) {
	t.Parallel()
	dep, _ := parseDeposit("9999999999*1000 UAH")
	if dep == nil {
		t.Errorf("Deposit is expected")
	} else {
		if dep.denomination.Cmp(new(big.Int).SetInt64(1000)) != 0 || dep.qty.Cmp(new(big.Int).SetInt64(9999999999)) != 0 || dep.currency != UAH {
			t.Errorf("Expected: %d*%d %s, got: %d*%d %s", 9999999999, 1000, UAH, dep.qty, dep.denomination, dep.currency)
		}
	}
}

var parseDepositsErrorsTable = []struct {
	in     string
	errMsg string
}{
	{"", "Parsing error: missing suffix ;. Incorrect value: "},
	{"1*10;", "Parsing error: expected two tokens separated by space. Incorrect value: 1*10"},
	{";", "Parsing error: unexpected prefix ;. Incorrect value: ;"},
	{"1*10 PLN U;", "Parsing error: expected two tokens separated by space. Incorrect value: 1*10 PLN U"},
	{"1*10 PLN;", "Parsing error: unsupported currency. Incorrect value: PLN"},
	{"1*2*10 UAH;", "Parsing error: expected two numbers separated by *. Incorrect value: 1*2*10"},
	{"1+10 UAH;", "Parsing error: expected two numbers separated by *. Incorrect value: 1+10"},
	{"*10 UAH;", "Parsing error: expected two numbers separated by *. Incorrect value: *10"},
	{"10* UAH;", "Parsing error: expected two numbers separated by *. Incorrect value: 10*"},
	{"10 UAH;", "Parsing error: expected two numbers separated by *. Incorrect value: 10"},
	{"f*10 UAH;", "Parsing error: qty is not valid. Incorrect value: f"},
	{"10*f UAH;", "Parsing error: denomination is not valid. Incorrect value: f"},
	{"10*12 UAH;", "Parsing error: denomination is not supported by this currency UAH. Incorrect value: 12"},
	{";10*12 UAH;", "Parsing error: unexpected prefix ;. Incorrect value: ;10*12 UAH;"},
	{";;", "Parsing error: unexpected prefix ;. Incorrect value: ;;"},
	{";;;", "Parsing error: unexpected prefix ;. Incorrect value: ;;;"},
	{"10*10 UAH", "Parsing error: missing suffix ;. Incorrect value: 10*10 UAH"},
	{"10*0 UAH;", "Parsing error: denomination cannot start with zero. Incorrect value: 0"},
	{"0*10 UAH;", "Parsing error: qty cannot start with zero. Incorrect value: 0"},
	{"-1*10 UAH;", "Parsing error: qty is not valid. Incorrect value: -1"},
	{"10*10 UAH ;", "Parsing error: expected two tokens separated by space. Incorrect value: 10*10 UAH "},
	{"10*10 UAH;;", "Parsing error: too many semicolons. Incorrect value: 10*10 UAH;;"},
	{"10*10 UAH;;;", "Parsing error: too many semicolons. Incorrect value: 10*10 UAH;;;"},
	{"10*10 UAH;3*5 USD;;", "Parsing error: too many semicolons. Incorrect value: 10*10 UAH;3*5 USD;;"},
	{"10*10 UAH;;3*5 USD;", "Parsing error: too many semicolons. Incorrect value: 10*10 UAH;;3*5 USD;"},
	{"10*10 UAH;3*500 USD;4*5 EUR;", "Parsing error: denomination is not supported by this currency USD. Incorrect value: 500"},
}

func TestParseDeposits(t *testing.T) {
	t.Parallel()
	for _, entry := range parseDepositsErrorsTable {
		deps, err := parseDeposits(entry.in)
		if deps != nil {
			t.Errorf("No deposits are expected")
		} else {
			verifyParsingError(t, entry.errMsg, err)
		}
	}
}

func TestDepositSum(t *testing.T) {
	qty := 99999999993
	denom := 100
	sum := qty * denom
	dep, err := parseDeposit(fmt.Sprintf("%d*%d USD", qty, denom))
	if err != nil {
		t.Errorf("Error is not expected. Got %s", err.Error())
	} else if dep.sum().Int64() != int64(sum) {
		t.Errorf("Expected %d, got %d", sum, dep.sum())
	}
}

var parseDepositsOkTable = []struct {
	in     string
	expUah string
	expUsd string
	expEur string
}{
	{"10*5 UAH;", "50", "0", "0"},
	{"14*20 USD;", "0", "280", "0"},
	{"25*50 EUR;", "0", "0", "1250"},
	{"10*5 UAH;13*10 USD;", "50", "130", "0"},
	{"10*10 UAH;3*5 UAH;", "115", "0", "0"},
	{"10*5 UAH;13*10 USD;9*100 EUR;", "50", "130", "900"},
	{"5*1000 UAH;9*100 USD;3*500 EUR;", "5000", "900", "1500"},
	{"10*10 UAH;3*5 EUR;3*5 UAH;", "115", "0", "15"},
	{"10*10 UAH;3*5 EUR;3*5 USD;4*5 UAH;", "120", "15", "15"},
	{"10*10 UAH;3*5 USD;3*5 USD;4*5 EUR;", "100", "30", "20"},
	{"10*10 UAH;3*5 USD;3*5 USD;4*5 EUR;", "100", "30", "20"},
	{"9223372036854775*20 UAH;3*5 USD;10*100 USD;30*5 EUR;15*500 EUR;", "184467440737095500", "1015", "7650"},
	{"9223372036854775807*2 UAH;3*5 USD;10*100 USD;30*5 EUR;15*500 EUR;", "18446744073709551614", "1015", "7650"},
	{"1*1 USD;2*2 USD;3*5 USD;4*10 USD;5*20 USD;6*50 USD;7*100 USD;1*1 USD;2*2 USD;3*5 USD;4*10 USD;5*20 USD;6*50 USD;7*100 USD;", "0", "2320", "0"},
	{"1*5 EUR;2*10 EUR;3*20 EUR;4*50 EUR;5*100 EUR;6*200 EUR;7*500 EUR;", "0", "0", "5485"},
	{"1*1 UAH;2*2 UAH;3*5 UAH;4*10 UAH;5*20 UAH;6*50 UAH;7*100 UAH;8*200 UAH;9*500 UAH;10*1000 UAH;", "17260", "0", "0"},
	{"1*5 EUR;2*10 EUR;3*20 EUR;4*50 EUR;1*1 USD;2*2 USD;3*5 USD;4*10 USD;5*20 USD;6*50 USD;7*100 USD;1*1 USD;2*2 USD;3*5 USD;4*10 USD;5*20 USD;6*50 USD;7*100 USD;5*100 EUR;6*200 EUR;7*500 EUR;1*1 UAH;2*2 UAH;3*5 UAH;4*10 UAH;5*20 UAH;6*50 UAH;7*100 UAH;8*200 UAH;9*500 UAH;10*1000 UAH;", "17260", "2320", "5485"},
	{"999999999999999999999999999999*1 USD;888888888888888888888888888*1 UAH;1111111111111111111111111111111111111111111111*5 EUR;", "888888888888888888888888888", "999999999999999999999999999999", "5555555555555555555555555555555555555555555555"},
}

func TestDepositsSum(t *testing.T) {
	t.Parallel()
	for _, entry := range parseDepositsOkTable {
		deps, err := parseDeposits(entry.in)
		if err == nil {
			uah := deps.sum(UAH).Text(10)
			if uah != entry.expUah {
				t.Errorf("Expected %s %s, got %s", entry.expUah, UAH, uah)
			}
			usd := deps.sum(USD).Text(10)
			if usd != entry.expUsd {
				t.Errorf("Expected %s %s, got %s", entry.expUsd, USD, usd)
			}
			eur := deps.sum(EUR).Text(10)
			if eur != entry.expEur {
				t.Errorf("Expected %s %s, got %s", entry.expEur, EUR, eur)
			}
		} else {
			t.Errorf("Error is not expected. Got %s", err.Error())
		}
	}
}

func verifyParsingError(t *testing.T, expectedErrorMsg string, err error) {
	if err == nil {
		t.Errorf("Error is expected")
	} else {
		if err.Error() != expectedErrorMsg {
			t.Errorf("Expected error msg: '%s'. Got: '%s'", expectedErrorMsg, err.Error())
		}
	}
}

func ExampleSession_ShowMenu() {
	session := CreateSession(EmptyAccount(), nil, nil)
	session.ShowMenu()
	// Output:
	// Choose next operation:
	// 1. Info.
	// 2. Deposit.
	// 3. Withdraw.
	// 4. Exit.
}

func TestExitSession(t *testing.T) {
	session := CreateSession(EmptyAccount(), nil, nil)
	session.exit()
	if session.finished != true {
		t.Error("Session is expected to be finished")
	}
}

func TestSession_IsFinished(t *testing.T) {
	session := CreateSession(EmptyAccount(), nil, nil)
	session.finished = true
	err := session.useChannel()
	if err == nil {
		t.Error("Error is expected")
	} else if err.Error() != "Error: session is finished" {
		t.Errorf("Expected err msg: '%s', got: '%s'", "Error: session is finished", err.Error())
	}
}

func TestDepositAccount(t *testing.T) {
	account := EmptyAccount()
	if new(big.Int).SetInt64(0).Cmp(account.balance(USD)) != 0 {
		t.Errorf("Expected %d balance in USD, but got %d", 0, account.balance(USD))
	}
	deps := deposits{}
	deps = append(deps, &deposit{UAH, new(big.Int).SetInt64(4), new(big.Int).SetInt64(20)})
	account.deposit(&deps)
	if new(big.Int).SetInt64(80).Cmp(account.balance(UAH)) != 0 {
		t.Errorf("Expected %d balance in UAH, but got %d", 0, account.balance(UAH))
	}
	account.deposit(&deps)
	if new(big.Int).SetInt64(160).Cmp(account.balance(UAH)) != 0 {
		t.Errorf("Expected %d balance in UAH, but got %d", 0, account.balance(UAH))
	}
}

func ExampleInfo() {
	session := CreateSession(EmptyAccount(), nil, nil)
	session.info()
	deps := deposits{}
	deps = append(deps, &deposit{UAH, new(big.Int).SetInt64(4), new(big.Int).SetInt64(20)})
	session.account.deposit(&deps)
	session.info()
	deps = append(deps, &deposit{USD, new(big.Int).SetInt64(3), new(big.Int).SetInt64(100)})
	deps = append(deps, &deposit{UAH, new(big.Int).SetInt64(3), new(big.Int).SetInt64(1000)})
	deps = append(deps, &deposit{UAH, new(big.Int).SetInt64(3), new(big.Int).SetInt64(1000)})
	deps = append(deps, &deposit{EUR, new(big.Int).SetInt64(5), new(big.Int).SetInt64(500)})
	session.account.deposit(&deps)
	session.info()
	// Output:
	// You have available:
	// 0 USD
	// 0 EUR
	// 0 UAH
	// You have available:
	// 0 USD
	// 0 EUR
	// 80 UAH
	// You have available:
	// 300 USD
	// 2500 EUR
	// 6160 UAH
}

func ExampleSession_Operate() {
	session := CreateSession(EmptyAccount(), make(chan bool), make(chan string))
	channel := session.valuesChannel
	go func() {
		_ = session.useChannel()
	}()
	go func() {
		channel <- "3"
		channel <- "4"
	}()
	for {
		finished := <-session.isFinishedChannel
		if finished {
			break
		}
	}
	close(session.valuesChannel)
	close(session.isFinishedChannel)
	// Output:
	//Withdraw
	//Choose next operation:
	//1. Info.
	//2. Deposit.
	//3. Withdraw.
	//4. Exit.
	//Bye!
}
