package money

import (
	"math/big"
	"testing"
)

func TestGetCurrency(t *testing.T) {
	var allowedCurrencyTable = []struct {
		in  string
		out Currency
	}{
		{"UAH", UAH},
		{"USD", USD},
		{"EUR", EUR},
		{"?", NONE},
	}
	t.Parallel()
	for _, entry := range allowedCurrencyTable {
		actualCurrency := ParseCurrency(entry.in)
		if !(actualCurrency == entry.out) {
			t.Errorf("Expected %s, got %s", entry.out, actualCurrency)
		}
	}
}

func TestDenominationIsAllowed(t *testing.T) {
	var allowedDenominationsTable = []struct {
		currency      Currency
		denominations []int64
	}{
		{USD, []int64{1, 2, 5, 10, 20, 50, 100}},
		{UAH, []int64{1, 2, 5, 10, 20, 50, 100, 200, 500, 1000}},
		{EUR, []int64{5, 10, 20, 50, 100, 200, 500}},
	}
	t.Parallel()
	for _, curDenoms := range allowedDenominationsTable {
		for _, d := range curDenoms.denominations {
			verifyDenominationIsAllowed(t, curDenoms.currency, d, true)
		}
	}
}

func TestDenominationIsNotAllowed(t *testing.T) {
	verifyDenominationIsAllowed(t, USD, 3, false)
	verifyDenominationIsAllowed(t, USD, 0, false)
	verifyDenominationIsAllowed(t, USD, -1, false)
	verifyDenominationIsAllowed(t, USD, 200, false)
	verifyDenominationIsAllowed(t, USD, 500, false)
	verifyDenominationIsAllowed(t, NONE, 1, false)
}

func verifyDenominationIsAllowed(t *testing.T, currency Currency, denom int64, isAllowed bool) {
	actual := currency.IsDenominationAllowed(new(big.Int).SetInt64(denom))
	if actual != isAllowed {
		t.Errorf("Denomination %d for Currency %s. isAllowed(), expected %t, actual %t", denom, currency, isAllowed, actual)
	}
}
