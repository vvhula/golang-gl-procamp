package money

import (
	"math/big"
)

const (
	UAH  Currency = "UAH"
	USD  Currency = "USD"
	EUR  Currency = "EUR"
	NONE Currency = ""
)

type (
	Currency string
)

func ParseCurrency(input string) Currency {
	switch {
	case input == string(UAH):
		return UAH
	case input == string(EUR):
		return EUR
	case input == string(USD):
		return USD
	}
	return NONE
}

func (currency Currency) IsValid() bool {
	return currency != NONE
}

func (currency Currency) IsDenominationAllowed(denomination *big.Int) bool {
	// Using strings to avoid int64 overflow(denomination variable)
	switch denomination.Text(10) {
	case "1", "2":
		return currency == USD || currency == UAH
	case "5", "10", "20", "50", "100":
		return currency == USD || currency == UAH || currency == EUR
	case "200", "500":
		return currency == EUR || currency == UAH
	case "1000":
		return currency == UAH
	}
	return false
}
